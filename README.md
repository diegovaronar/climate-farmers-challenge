# Climate Farmers Challenge


## STEP 1 - Region selection
The shapefile has been downloaded from (https://centrodedescargas.cnig.es/) and stored in `/data/regions_spain`. This file has been then filtered to select Burgos region, located in northern Spain. This region has been used to clip the datasets.

![BurgosLocation](/src/images/burgos_region.png "Burgos Location")

## STEP 2 - Data Acquisition
The climate and land-cover datasets have been dowloaded through CDS API. The download has been automated, defining some functions included in `/src/api_download.py`. The soil map has been downloaded manually from (https://soilgrids.org/), selecting mean SOC stock values.

These are the variables that have been selected from the ERA5-Land monthly data:
- 2m temperature (K)
- Total precipitation (m/day)
- Evaporation from bare soil (m/day) 
- Evaporation from open water surfaces excluding oceans (m/day)
- Evaporation from vegetation transpiration (m/day)

This variables have been processed in order to standardize the units and facilitate the visualization (see `/analysis.ipynb`).

## STEP 3 - Integration

The climate dataset has the lowest resolution (9 km). The land-cover and soil datasets have a 300 m and 250 m resolution respectively and have been, therefore, reprojected to match the lowest resolution. The values have been aggregated following different resampling methods depending on the variable type, continuous (SOC) or categorical (land-cover).

More details in `/analysis.ipynb`

## STEP 4 - Analysis and Visualization

![barplotlandcover](/src/images/barplot_lc.png "Land Cover")

![lineplottemp](/src/images/lineplot_temp.png "Temperature")

![lineplot_tp](/src/images/lineplot_tp.png "Precipitation")

More details in `/analysis.ipynb`

## STEP 5 - Sampling Design

![Samplesize](/src/images/sample_size.png "Sample Size")

Where:
- Z = 1.96 (95% confidence interval)
- σ = 13.61 (based on values from soil map)
- e = 0.1 (10% error)
- μ = 46.42 (based on values from soil map)

**n >= 34** (see analysis.ipynb for details on the full calculation)



#Import libraries
import cdsapi
import zipfile
import os

def landcover_map(year, path):
    """
    download_landcover_map downloads land-cover classification gridded map for a specified year in NetCDF4 format

    :param year: year when land_cover classification was performed
    :param path: path where the file will be stored + filename
    """
    c = cdsapi.Client()
    c.retrieve(
        'satellite-land-cover',
        {
            'variable': 'all',
            'format': 'zip',
            'year': str(year),
            'version': 'v2.1.1',
        },
        path + '.zip')
    with zipfile.ZipFile(path + '.zip','r') as zip_ref:
        zip_ref.extractall('Data/')
    zip_ref.close()
    os.remove(path + '.zip')
    print("Successful download.")
    
def climate_data(start_year, end_year, region, path):
    """
    download_climate_data downloads ERA5-Land monthly climate gridded data for a specified period of years inside a specified region in NetCDF4 format

    :param start_year: first year of the series
    :param end_year: last year of the series
    :region: bounding box (geodataframe.total_bounds) for the specified region
    :path: path where the file will be stored + filename
    """
    xmin, xmax, ymin, ymax = region.total_bounds[0], region.total_bounds[2], region.total_bounds[1], region.total_bounds[3]
    c = cdsapi.Client()
    c.retrieve(
        'reanalysis-era5-land-monthly-means',
        {
            'variable': [
                '2m_temperature', 'total_precipitation', 'evaporation_from_bare_soil', 
                'evaporation_from_open_water_surfaces_excluding_oceans', 'evaporation_from_vegetation_transpiration'
            ],
            'year': [str(x) for x in [*range(start_year, end_year + 1, 1)]],
            'month': [
                '01', '02', '03', '04', '05', '06','07', '08', '09', '10', '11', '12',
            ],
            'time': '00:00',
            'area': [
                ymax, xmin, ymin, xmax,
            ],
            'format': 'netcdf.zip',
            'product_type': 'monthly_averaged_reanalysis',
        },
        path + '.netcdf.zip')
    with zipfile.ZipFile(path + '.netcdf.zip','r') as zip_ref:
        zip_ref.extractall('Data/')
    zip_ref.close()
    os.remove(path + '.netcdf.zip')
    print("Successful download")